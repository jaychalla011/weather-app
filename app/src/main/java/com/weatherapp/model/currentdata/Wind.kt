package com.weatherapp.model.currentdata

data class Wind(
    val deg: Int,
    val speed: Double
)