package com.weatherapp.model.currentdata

data class CurrentWeatherData(
    val base: String,
    val cod: String,
    val dt: String,
    val id: String,
    val main: Main,
    val name: String,
    val timezone: String,
    val visibility: String

)