package com.weatherapp.model.futuredata

data class X(
    val clouds: Clouds,
    val dt: String,
    val dt_txt: String,
    val main: Main,
    val weather: List<Weather>
)