package com.weatherapp.model.futuredata

data class FutureWeatherData(
//    val city: City,
    val cnt: String,
    val cod: String,
    val list: List<X>,
    val message: String
)