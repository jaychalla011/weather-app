package com.weatherapp.dagger.module.activity

import com.weatherapp.dagger.module.headerdata.HeaderData
import com.weatherapp.retrofit.ApiService
import com.weatherapp.retrofit.DataManager
import com.weatherapp.viewmodel.HomeActivityViewModel
import dagger.Module
import dagger.Provides

@Module
class HomeModule {
    @Provides
    fun homeViewModelProvider(
        apiService: ApiService,
        dataManager: DataManager,
        headerData: HeaderData
    ): HomeActivityViewModel {
        return HomeActivityViewModel(apiService, dataManager, headerData)
    }
}