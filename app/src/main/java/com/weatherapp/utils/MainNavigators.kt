package com.weatherapp.utils

interface MainNavigators {
    fun onSuccess(any: Any)
    fun onError(error: String)
}