package com.weatherapp.dagger.module
import com.weatherapp.dagger.module.headerdata.HeaderData
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class HeaderModule {

    @Provides
    @Singleton
    fun provideHeader(): HeaderData {
        return HeaderData()
    }
}