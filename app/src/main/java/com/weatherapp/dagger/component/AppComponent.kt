package com.weatherapp.dagger.component
import android.app.Application
import com.weatherapp.App
import com.weatherapp.dagger.builder.ActivityBuilder
import com.weatherapp.dagger.module.AppModule
import com.weatherapp.dagger.module.HeaderModule
import com.weatherapp.dagger.module.NetworkModule

import dagger.BindsInstance
import dagger.Component


import javax.inject.Singleton


@Singleton
@Component(modules = [ ActivityBuilder::class, NetworkModule::class, AppModule::class, HeaderModule::class  ])
interface AppComponent  {

    fun inject(app: App)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}