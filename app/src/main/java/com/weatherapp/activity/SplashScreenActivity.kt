package com.weatherapp.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.databinding.DataBindingUtil
import com.weatherapp.R
import com.weatherapp.base.BaseActivity
import com.weatherapp.databinding.ActivitySplashScreenBinding
import com.weatherapp.utils.DURATION_3000


class SplashScreenActivity : BaseActivity() {

    lateinit var binding : ActivitySplashScreenBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this@SplashScreenActivity,R.layout.activity_splash_screen)
        handler()
    }

    fun handler(){
        Handler().postDelayed({
                val mainIntent = Intent(this@SplashScreenActivity, HomeActivity::class.java)
                this@SplashScreenActivity.startActivity(mainIntent)
                this@SplashScreenActivity.finish()

        }, DURATION_3000)
    }
}
