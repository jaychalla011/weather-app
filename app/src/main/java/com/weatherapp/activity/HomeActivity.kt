package com.weatherapp.activity

import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.weatherapp.R
import com.weatherapp.adapter.WeatherAdapter
import com.weatherapp.base.BaseActivity
import com.weatherapp.databinding.ActivityHomeBinding
import com.weatherapp.model.currentdata.CurrentWeatherData
import com.weatherapp.model.futuredata.FutureWeatherData
import com.weatherapp.model.futuredata.X
import com.weatherapp.utils.ItemClick
import com.weatherapp.utils.MainNavigators
import com.weatherapp.utils.Utils.kelvinToCelsius
import com.weatherapp.viewmodel.HomeActivityViewModel
import javax.inject.Inject


class HomeActivity : BaseActivity(), MainNavigators, ItemClick {

    @Inject
    lateinit var viewModel: HomeActivityViewModel

    lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this@HomeActivity, R.layout.activity_home)
        viewModel!!.setNavigator(this)
        binding.homeViewModel = viewModel

        binding.retryBtn.setOnClickListener {
            binding.progressCircular.visibility = View.VISIBLE
            binding.noTv.visibility = View.GONE
            binding.retryBtn.visibility = View.GONE
            binding.mainLl.visibility = View.GONE

            viewModel!!.getCurrentData()
            viewModel!!.getFutureData()

        }

        viewModel!!.getCurrentData()
        viewModel!!.getFutureData()
    }

    override fun onSuccess(any: Any) {
        if (any != null) {
            if (any is CurrentWeatherData) {
                var currentWeatherData = any as CurrentWeatherData
                setCurrentData(currentWeatherData)
            } else if (any is FutureWeatherData) {
                var futureWeatherData = any as FutureWeatherData
                setFutureData(futureWeatherData)
            }
        }
    }

    private fun setFutureData(futureWeatherData: FutureWeatherData) {

        binding.progressCircular.visibility = View.GONE
        binding.noTv.visibility = View.GONE
        binding.retryBtn.visibility = View.GONE
        binding.mainLl.visibility = View.VISIBLE


        var list = futureWeatherData.list
        var newList = list.distinctBy { it.dt_txt.substring(0, 10) }
        newList = newList.subList(1, newList.size)
        binding.recyceler.adapter = WeatherAdapter(this, newList, this)
    }

    private fun setCurrentData(currentWeatherData: CurrentWeatherData) {

        binding.temp.text = Math.round(kelvinToCelsius(currentWeatherData.main.temp)).toString()
        //binding.date.text = Utils.getDate(currentWeatherData.dt,"dd-MMM-yyyy").substring(0,5)
        binding.progressCircular.visibility = View.GONE
        binding.noTv.visibility = View.GONE
        binding.retryBtn.visibility = View.GONE
        binding.mainLl.visibility = View.VISIBLE
    }

    override fun onError(error: String) {
        binding.progressCircular.visibility = View.GONE
        binding.noTv.visibility = View.VISIBLE
        binding.retryBtn.visibility = View.VISIBLE
        binding.mainLl.visibility = View.GONE
        Toast.makeText(this@HomeActivity, error, Toast.LENGTH_LONG).show()
    }

    override fun itemclick(int: Int, any: Any?) {
        var data = any as X
        binding.temp.text = Math.round(kelvinToCelsius(data.main.temp)).toString()
        binding.date.text = data.dt_txt.substring(8, 10) + "- Feb"

    }
}
