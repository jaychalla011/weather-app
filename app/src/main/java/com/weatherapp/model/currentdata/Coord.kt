package com.weatherapp.model.currentdata

data class Coord(
    val lat: Double,
    val lon: Double
)