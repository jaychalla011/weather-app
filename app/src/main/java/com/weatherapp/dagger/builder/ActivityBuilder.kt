package com.weatherapp.dagger.builder

import com.weatherapp.activity.HomeActivity
import com.weatherapp.activity.SplashScreenActivity
import com.weatherapp.dagger.module.activity.HomeModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector()
    abstract fun splashActivity() : SplashScreenActivity

    @ContributesAndroidInjector(modules = [(HomeModule::class)])
    abstract fun homeActivity(): HomeActivity

}