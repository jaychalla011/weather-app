package com.weatherapp.base;

import android.app.Application
import androidx.databinding.ObservableBoolean

import androidx.lifecycle.ViewModel
import com.weatherapp.dagger.module.headerdata.HeaderData
import com.weatherapp.retrofit.ApiService
import com.weatherapp.retrofit.DataManager

import io.reactivex.disposables.CompositeDisposable


import java.lang.ref.WeakReference

abstract class BaseViewModel<N>: ViewModel {

    private var mDataManager: DataManager?=null
    private var mApiService: ApiService?=null
    private var mcontext: Application?=null
    private val mIsLoading = ObservableBoolean(false)
    private val mCompositeDisposable: CompositeDisposable?=null
    private var header : HeaderData?=null

    private var mNavigator: WeakReference<N>? = null

    constructor(dataManager: DataManager,apiService: ApiService,headerData: HeaderData?){
        mDataManager = dataManager
        mApiService = apiService
        header = headerData
    }

    override fun onCleared() {
        mCompositeDisposable!!.dispose()
        super.onCleared()
    }

    fun getHeader():HeaderData?
    {
        return header
    }

    fun getCompositeDisposable(): CompositeDisposable? {
        return mCompositeDisposable
    }


    fun getDataManager(): DataManager? {
        return mDataManager
    }

    fun getApiService():ApiService?{
        return  mApiService
    }


    fun getIsLoading(): ObservableBoolean {
        return mIsLoading
    }

    fun setIsLoading(isLoading: Boolean) {
        mIsLoading.set(isLoading)
    }

    fun getNavigator(): N? {
        return mNavigator!!.get()
    }



    fun setNavigator(navigator: N) {
        this.mNavigator = WeakReference(navigator)
    }

    fun getNavigation(): N? {
        return mNavigator!!.get()
    }



    fun setNavigation(navigator: N) {
        this.mNavigator = WeakReference(navigator)
    }

}