package com.weatherapp.utils

import java.text.SimpleDateFormat
import java.util.logging.SimpleFormatter

object Utils {
    fun kelvinToCelsius(kel : Double) : Double{
        var sal = kel - 273.15
        return sal
    }
    fun getDate(data :String ,formate : String ): String{
        val formate = SimpleDateFormat(formate)
        return  formate.format(data)
    }
}