package com.weatherapp.model.futuredata

data class Coord(
    val lat: Double,
    val lon: Double
)