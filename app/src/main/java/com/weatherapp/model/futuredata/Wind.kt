package com.weatherapp.model.futuredata

data class Wind(
    val deg: String,
    val speed: Double
)