package com.weatherapp.model.currentdata

data class Main(
    val feels_like: Double,
    val humidity: String,
    val pressure: String,
    val temp: Double,
    val temp_max: Double,
    val temp_min: Double
)