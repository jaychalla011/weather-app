package com.weatherapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.weatherapp.R
import com.weatherapp.databinding.RcItemsBinding
import com.weatherapp.model.futuredata.X
import com.weatherapp.utils.ItemClick
import com.weatherapp.utils.Utils
import kotlinx.android.synthetic.main.rc_items.view.*

class WeatherAdapter(var context : Context, var items: List<X>, var itemClick: ItemClick) :
    RecyclerView.Adapter<WeatherAdapter.ViewHolder>() {

    var binding: RcItemsBinding? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.rc_items, parent, false)
        return ViewHolder(binding!!)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.temp.text =
            Math.round(Utils.kelvinToCelsius(items[position].main.temp)).toString()
        holder.itemView.date.text =
            items[position].dt_txt.substring(8, 10) + "- Feb"

        holder.itemView.setOnClickListener {
            val slide_up = AnimationUtils.loadAnimation(
                context,
                R.anim.slide_up
            )
            slide_up.startOffset = (10 * position).toLong()
            holder.itemView.startAnimation(slide_up)
            itemClick.itemclick(position, items[position])
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }


    class ViewHolder(binding: RcItemsBinding) : RecyclerView.ViewHolder(binding.root) {

    }
}