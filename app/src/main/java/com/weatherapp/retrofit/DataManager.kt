package com.weatherapp.retrofit;

interface DataManager  {

    fun updateUserInfo(
            accessToken: String,
            userName: String,
            email: String,
            profilePicPath: String)


}