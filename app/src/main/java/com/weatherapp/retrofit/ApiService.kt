package com.weatherapp.retrofit;

import com.google.gson.JsonObject
import com.weatherapp.model.currentdata.CurrentWeatherData
import com.weatherapp.model.futuredata.FutureWeatherData
import com.weatherapp.utils.API_KEY

import okhttp3.ResponseBody

import retrofit2.Call
import retrofit2.http.*
import retrofit2.http.POST



interface ApiService {

    @GET("weather?q=bangalore&appid="+ API_KEY)
    fun getCurrentData() : Call<CurrentWeatherData>

    @GET(" forecast?q=bangalore&cnt=40&appid="+ API_KEY)
    fun getFutureData() : Call<FutureWeatherData>

}