package com.weatherapp.utils

import com.weatherapp.retrofit.DataManager
import javax.inject.Inject

class AppDataManager : DataManager {


    private var mPreferencesHelper: PreferencesHelper? = null

    @Inject
    constructor(preferencesHelper: PreferencesHelper)
    {
        mPreferencesHelper = preferencesHelper
    }

    override fun updateUserInfo(AccessToken: String, userName: String, email: String, profilePicPath: String) {
        accessToken = AccessToken

    }

     var accessToken: String
        get() = mPreferencesHelper!!.accessToken
        set(accesstoken) {
            mPreferencesHelper!!.accessToken = accesstoken
        }
}