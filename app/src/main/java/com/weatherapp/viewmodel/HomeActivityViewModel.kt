package com.weatherapp.viewmodel

import com.weatherapp.base.BaseViewModel
import com.weatherapp.dagger.module.headerdata.HeaderData
import com.weatherapp.model.currentdata.CurrentWeatherData
import com.weatherapp.model.futuredata.FutureWeatherData
import com.weatherapp.retrofit.ApiService
import com.weatherapp.retrofit.DataManager
import com.weatherapp.utils.MainNavigators
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeActivityViewModel(
    apiService: ApiService,
    dataManager: DataManager,
    headerData: HeaderData
) : BaseViewModel<MainNavigators>(
    dataManager, apiService, headerData
) {

    fun getCurrentData() {
        var call = getApiService()!!.getCurrentData()
        call.enqueue(object : Callback<CurrentWeatherData> {
            override fun onFailure(call: Call<CurrentWeatherData>, t: Throwable) {
                getNavigator()!!.onError("Something went wrong")
            }

            override fun onResponse(
                call: Call<CurrentWeatherData>,
                response: Response<CurrentWeatherData>
            ) {
                getNavigator()!!.onSuccess(response.body()!!)
            }

        })
    }


    fun getFutureData(){
        var call = getApiService()!!.getFutureData()
        call.enqueue(object : Callback<FutureWeatherData> {
            override fun onFailure(call: Call<FutureWeatherData>, t: Throwable) {
                getNavigator()!!.onError("Something went wrong")
            }

            override fun onResponse(
                call: Call<FutureWeatherData>,
                response: Response<FutureWeatherData>
            ) {
                getNavigator()!!.onSuccess(response.body()!!)
            }

        })
    }

}